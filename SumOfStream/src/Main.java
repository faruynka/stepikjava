import java.io.*;
import java.nio.charset.StandardCharsets;

public class Main {
    public static void main(String[] args) throws IOException {
        InputStream inputStream = System.in;
        getSumOfStream(inputStream);
    }

    public static void getSumOfStream(InputStream inputStream) throws IOException {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        int blocksize;
        while ((blocksize = inputStream.read()) != -1) {
            outputStream.write(blocksize);
        }
        StringBuilder inputString = new StringBuilder(new String(outputStream.toByteArray(), StandardCharsets.UTF_8));
        double sum = 0.0;
        for (int i = 0; i < inputString.length(); i++) {
            for (int j = i; j < inputString.length(); j++) {
                double tmp = 0;
                if (inputString.charAt(j) == ' ' || inputString.charAt(j) == '\n') {
                    char[] charArray = new char[j - i];
                    inputString.getChars(i, j, charArray, 0);
                    try {
                        tmp = Double.parseDouble(String.valueOf(charArray));
                    } catch (Exception e) {

                    }
                    sum += tmp;
                    i = j;
                    break;
                } else if (j == inputString.length() - 1 && inputString.charAt(j) != ' '
                        && inputString.charAt(j) != '\n') {
                    if (j - i > 0) {
                        char[] charArray = new char[j - i + 1];
                        inputString.getChars(i, j + 1, charArray, 0);
                        try {
                            tmp = Double.parseDouble(String.valueOf(charArray));
                        } catch (Exception e) {

                        }
                        i = j;
                    } else {
                        try {
                            tmp = Double.parseDouble(String.valueOf(inputString.charAt(j)));
                        } catch (Exception e) {

                        }
                    }
                    sum += tmp;
                }
            }
        }
        System.out.format("%.6f", sum);
    }
}

