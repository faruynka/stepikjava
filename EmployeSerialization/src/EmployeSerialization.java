import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.GregorianCalendar;

public class EmployeSerialization {
    public static void main(String[] args) throws IOException {
        int numberOfWorkers = 3;
        Employe[] employes = new Employe[numberOfWorkers];
        employes[0] = new Employe("Jack", "manager", 60000,
                new GregorianCalendar(2008, 7, 11));
        employes[1] = new Employe("John", "programmer", 120000,
                new GregorianCalendar(2013, 6, 9));
        employes[2] = new Employe("Lena", "economist", 40000,
                new GregorianCalendar(2011, 10, 4));
        System.out.println(Arrays.toString(employes));
        writeEmployes(employes, "C:/Users/User/Desktop/Test/inEmploye.txt");
        System.out.println(Arrays.toString(readEmployes("C:/Users/User/Desktop/Test/inEmploye.txt")));
    }

    public static byte[] serializedEmployes(Employe[] employes) throws IOException {
        byte[] byteArray;
        int numberOfWorkers = employes.length;
        try (ByteArrayOutputStream output = new ByteArrayOutputStream();
             ObjectOutputStream oos = new ObjectOutputStream(output)) {
            oos.writeInt(numberOfWorkers);
            for (int i = 0; i < numberOfWorkers; i++) {
                oos.writeObject(employes[i]);
            }
            byteArray = output.toByteArray();
        } catch (IOException e) {
            throw e;
        }
        return byteArray;
    }

    public static Employe[] deserializeAnimalArray(byte[] data) throws IllegalArgumentException {
        Employe[] res;
        try (ByteArrayInputStream arr = new ByteArrayInputStream(data);
             ObjectInputStream ois = new ObjectInputStream(arr)) {
            int length = ois.readInt();
            res = new Employe[length];
            for (int i = 0; i < length; i++) {
                Employe deserializedAnimal = (Employe) ois.readObject();
                if (deserializedAnimal instanceof Employe) {
                    res[i] = deserializedAnimal;
                } else {
                    throw new IllegalArgumentException();
                }
            }
        } catch (Exception e) {
            throw new IllegalArgumentException();
        }
        return res;
    }

    public static Employe[] readEmployes(String inputPath) throws IOException {
        byte[] arr;
        try (InputStream inputStream = Files.newInputStream(Paths.get(inputPath))) {
            arr = inputStream.readAllBytes();
        } catch (IOException e) {
            throw e;
        }
        return deserializeAnimalArray(arr);
    }

    public static void writeEmployes(Employe[] employes, String outputPath) throws IOException {
        byte data[] = serializedEmployes(employes);
        try (InputStream inputStream = new ByteArrayInputStream(data);
             OutputStream outputStream = Files.newOutputStream(Paths.get(outputPath))) {
            int blockSize;
            while ((blockSize = inputStream.read()) != -1) {
                outputStream.write(blockSize);
            }
        } catch (IOException e) {
            throw e;
        }
    }
}
