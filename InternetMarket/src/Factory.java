public class Factory {
    public static Product productName(String product) {
        if (product.equals("Egg")) {
            return new Egg();
        } else if (product.equals("Jacket")) {
            return new Jacket();
        } else if (product.equals("Ball")) {
            return new Ball();
        } else if (product.equals("Fridge")) {
            return new Fridge();
        } else {
            throw new IllegalArgumentException("Wrong name of product");
        }
    }
}
