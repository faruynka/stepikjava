public class Task4 {
    public static void main(String arg[]) {
        System.out.println(flipBit(3, 3));
    }

    public static int flipBit(int value, int bitIndex) {
        return value ^ 1 << bitIndex - 1;
    }
}
