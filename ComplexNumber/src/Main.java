public class Main {
    public static void main(String[] args) {
        ComplexNumber c1 = new ComplexNumber(1, 3);
        ComplexNumber c2 = new ComplexNumber(4, 3);
        System.out.println(c1.add(c2).toString());
        System.out.println(c1.equals(c2));
        System.out.println(c1.abs());
        System.out.println(c1.div(c2));
        System.out.println(c1.hashCode());
        System.out.println(c1.mult(c2));
    }
}
