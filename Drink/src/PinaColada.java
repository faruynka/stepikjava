public class PinaColada extends HotDrink {
    @Override
    public String uniqueRecipePart() {
        return "Milk, Pineapple, Ice Cream, Coconut drink";
    }

    @Override
    public String getName() {
        return "Pina Colada";
    }
}
