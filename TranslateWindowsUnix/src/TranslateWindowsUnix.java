import java.io.IOException;
import java.io.InputStream;

public class TranslateWindowsUnix {
    public static void main(String[] args) throws IOException {
        InputStream inputStream = System.in;
        int prevElement = inputStream.read();
        int nextElement = inputStream.read();
        while (prevElement != -1) {
            if (prevElement == 13 && nextElement == 10) {
                System.out.write(nextElement);
                prevElement = inputStream.read();
            } else {
                System.out.write(prevElement);
                prevElement = nextElement;
            }
            nextElement = inputStream.read();
        }
        System.out.flush();
    }
}

