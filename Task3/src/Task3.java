public class Task3 {
    public static void main(String arg[]) {
        System.out.println(doubleExpression(3, 3, 1));
    }

    public static boolean doubleExpression(double a, double b, double c) {
        return Math.abs((a + b) - c) < 0.0001;
    }
}