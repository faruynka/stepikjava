public interface Product {
    String name();

    double price();

    Size size();
}
