public class Fridge implements Product {
    @Override
    public Size size() {
        return new Size(35, 35, 140);
    }

    @Override
    public double price() {
        return 256.3;
    }

    @Override
    public String name() {
        return "Fridge";
    }
}
