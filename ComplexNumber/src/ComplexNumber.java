public class ComplexNumber {
    private final double re;
    private final double im;

    public ComplexNumber(double re, double im) {
        this.re = re;
        this.im = im;
    }

    public double getRe() {
        return re;
    }

    public double getIm() {
        return im;
    }

    public ComplexNumber add(ComplexNumber c) {
        return new ComplexNumber(re + c.re, im + c.im);
    }

    public ComplexNumber subtract(ComplexNumber c) {
        return new ComplexNumber(re - c.re, im - c.im);
    }

    public ComplexNumber mult(ComplexNumber c) {
        return new ComplexNumber(re * c.re - im * c.im, im * c.re + re * c.im);
    }

    public ComplexNumber div(ComplexNumber c) {
        return new ComplexNumber((re * c.re + im * c.im) / (c.re * c.re + c.im * c.im),
                (im * c.re - re * c.im) / (c.re * c.re + c.im * c.im));
    }

    public double abs() {
        return Math.sqrt(re * re + im * im);
    }

    public double arg() {
        return Math.acos(re / abs());
    }

    public ComplexNumber pow(double n) {
        double nArg = n * arg();
        double absPowN = Math.pow(abs(), n);
        return new ComplexNumber(absPowN * Math.cos(nArg), absPowN * Math.sin(nArg));
    }

    @Override
    public String toString() {
        return Double.toString(re) + " + i * " + Double.toString(im);
    }

    @Override
    public boolean equals(Object c) {
        if (this == c) {
            return true;
        }
        if (c instanceof ComplexNumber) {
            ComplexNumber other = (ComplexNumber) c;
            if (other.re == re && other.im == im) {
                return true;
            }
        }
        return false;
    }

    @Override
    public int hashCode() {
        return Double.hashCode(re) + Double.hashCode(im);
    }
}
