public abstract class HotDrink implements DrinkRecipe {
    @Override
    public String recipe() {
        return "A recipe of " + getName() + " is: Rum, Ice, " + uniqueRecipePart();
    }

    public abstract String uniqueRecipePart();

    public abstract String getName();
}
