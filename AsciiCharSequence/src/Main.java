public class Main {
    public static void main(String[] args) {
        byte[] array = {1,4,5,23,12,4};
        AsciiCharSequence str = new AsciiCharSequence(array);
        System.out.println(str.toString());
        System.out.println(str.length());
        System.out.println(str.charAt(1));
        System.out.println(str.subSequence(1,3));
    }
}
