import java.util.ArrayList;

public class Order {
    private ArrayList<Product> products = new ArrayList();

    public double priceOrder() {
        double sum = 0;
        for (Product product : products) {
            sum += product.price();
        }
        return sum;
    }

    public Size packetSize() {
        int x = 0;
        int y = 0;
        int z = 0;
        for (Product product : products) {
            if (product.size().x > x) {
                x = product.size().x;
            }
            if (product.size().z > z) {
                z = product.size().z;
            }
            y += product.size().y;
        }
        return new Size(x, y, z);
    }

    public ArrayList<Product> addProduct(Product product) {
        products.add(product);
        return products;
    }
}
