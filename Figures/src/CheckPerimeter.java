public class CheckPerimeter implements Processor {
    private double c2;
    private double c3;
    private double c4;
    private double c5;
    private double c6;
    private double θ;


    public CheckPerimeter(double c2, double c3, double c4, double c5, double c6, double θ) {
        this.c2 = c2;
        this.c3 = c3;
        this.c4 = c4;
        this.c5 = c5;
        this.c6 = c6;
        this.θ = θ;

    }

    @Override
    public Figure processFigure(Figure figure) {
        if (c3 < 1) {
            throw new IllegalArgumentException("unction required coefficient from 1 to infinity");
        }
        if (figure.perimeter() > c2) {
            figure.sizeChanging(c3);
            figure.figureShift(Shift.RIGHT, c4);
            figure.figureShift(Shift.UP, c5);
        } else {
            figure.sizeChanging(1 / c3);
            figure.rotate(c6);
        }
        return figure;
    }
}
