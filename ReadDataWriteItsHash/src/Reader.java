import java.io.File;
import java.io.IOException;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.Scanner;
import java.util.concurrent.atomic.AtomicBoolean;

public class Reader extends Thread {
    private ArrayBlockingQueue<String> readerHasherBuffer;
    private String path;
    private AtomicBoolean readerStopped;

    public Reader(ArrayBlockingQueue<String> readerHasherBuffer, String path, AtomicBoolean readerStopped) {
        this.readerHasherBuffer = readerHasherBuffer;
        this.path = path;
        this.readerStopped = readerStopped;
    }

    @Override
    public void run() {
        try {
            Scanner scan = new Scanner(new File(path));
            while (scan.hasNext()) {
                String line = scan.next();
                synchronized (readerHasherBuffer) {
                    while (readerHasherBuffer.remainingCapacity() == 0) {
                        readerHasherBuffer.wait();
                    }
                    readerHasherBuffer.add(line);
                    readerHasherBuffer.notify();
                }
            }
            synchronized (readerHasherBuffer) {
                readerStopped.set(true);
                readerHasherBuffer.notify();
            }
        } catch (IOException e) {
            System.out.println(e.getMessage());
        } catch (InterruptedException e) {
            System.out.println(e.getMessage());
        }
    }
}