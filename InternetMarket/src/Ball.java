public class Ball implements Product {
    @Override
    public Size size() {
        return new Size(4, 4, 4);
    }

    @Override
    public double price() {
        return 13.4;
    }

    @Override
    public String name() {
        return "Ball";
    }
}