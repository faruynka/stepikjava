public class Triangle implements Figure {
    private Point p1;
    private Point p2;
    private Point p3;
    private double a = p1.distTo(p2);
    private double b = p1.distTo(p3);
    private double c = p2.distTo(p3);

    public Triangle(Point p1, Point p2, Point p3) {
        this.p1 = p1;
        this.p2 = p2;
        this.p3 = p3;
    }

    @Override
    public String toString() {
        return "bottomLeftPoint: [" + p1.getX() + ", " + p1.getY() + "]" + '\n'
                + "upLeftPoint: [" + p2.getX() + ", " + p2.getY() + "]" + '\n'
                + "upRightPoint: [" + p3.getX() + ", " + p3.getY() + "]";
    }

    @Override
    public void sizeChanging(double coefficient) {
        if (coefficient < 0) {
            throw new IllegalArgumentException("Coefficient is a negative number");
        } else if (coefficient > 1) {
            p2 = new Point(p2.getX(), a * coefficient - a + p2.getY());
            p3 = new Point(b * coefficient - b + p3.getX(), p3.getY());
        } else if (coefficient < 1 && coefficient > 0) {
            p2 = new Point(p2.getX(), p2.getY() - (a - a * coefficient));
            p3 = new Point(p3.getX() - (b - b * coefficient), p3.getY());
        }
    }

    @Override
    public double perimeter() {
        return a + b + c;
    }

    @Override
    public double square() {
        double p = perimeter() / 2;
        return Math.sqrt(p * (p - a) * (p - b) * (p - c));
    }

    @Override
    public void figureShift(Shift direction, double shift) { //coefficient bad name
        if (direction.equals(Shift.RIGHT)) {
            p1 = new Point(p1.getX() + shift, p1.getY());
            p2 = new Point(p2.getX() + shift, p2.getY());
            p3 = new Point(p3.getX() + shift, p3.getY());
        } else if (direction.equals(Shift.LEFT)) {
            p1 = new Point(p1.getX() - shift, p1.getY());
            p2 = new Point(p2.getX() - shift, p2.getY());
            p3 = new Point(p3.getX() - shift, p3.getY());
        } else if (direction.equals(Shift.UP)) {
            p1 = new Point(p1.getX(), p1.getY() + shift);
            p2 = new Point(p2.getX(), p2.getY() + shift);
            p3 = new Point(p3.getX(), p3.getY() + shift);
        } else {
            p1 = new Point(p1.getX(), p1.getY() - shift);
            p2 = new Point(p2.getX(), p2.getY() - shift);
            p3 = new Point(p3.getX(), p3.getY() - shift);
        }
    }

    @Override
    public void rotate(double θ) {
        p1 = new Point(p1.getX() * Math.cos(θ) - p1.getY() * Math.sin(θ),
                p1.getX() * Math.sin(θ) + p1.getY() * Math.cos(θ));
        p2 = new Point(p2.getX() * Math.cos(θ) - p2.getY() * Math.sin(θ),
                p2.getX() * Math.sin(θ) + p2.getY() * Math.cos(θ));
        p3 = new Point(p3.getX() * Math.cos(θ) - p3.getY() * Math.sin(θ),
                p3.getX() * Math.sin(θ) + p3.getY() * Math.cos(θ));
    }
}
