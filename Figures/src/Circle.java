public class Circle implements Figure {
    private Point center;
    private double radius;

    public Circle(Point center, double radius) {
        this.center = center;
        this.radius = radius;
    }

    @Override
    public String toString() {
        return "A center: [" + center.getX() + ", " + center.getY() + "], radius: " + radius;
    }

    @Override
    public void sizeChanging(double coefficient) {
        if (coefficient < 0) {
            throw new IllegalArgumentException("Coefficient is a negative number");
        } else if (coefficient > 0 && coefficient != 1) {
            radius = radius * coefficient;
        }
    }

    @Override
    public double perimeter() {
        return 2 * Math.PI * radius;
    }

    @Override
    public double square() {
        return Math.PI * radius * radius;
    }

    public void figureShift(Shift direction, double shift) {
        if (direction.equals(Shift.RIGHT)) {
            center = new Point(center.getX() + shift, center.getY());
        } else if (direction.equals(Shift.LEFT)) {
            center = new Point(center.getX() - shift, center.getY());
        } else if (direction.equals(Shift.UP)) {
            center = new Point(center.getX(), center.getY() + shift);
        } else {
            center = new Point(center.getX(), center.getY() - shift);
        }
    }
    @Override
    public void rotate(double θ){
        center = new Point(center.getX() * Math.cos(θ) - center.getY() * Math.sin(θ),
                center.getX() * Math.sin(θ) + center.getY() * Math.cos(θ));
    }
}