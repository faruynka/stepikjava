public class Main {
    public static void main(String[] args) {
        TextAnalyzer[] analyzers = {new TooLongTextAnalyzer(50),
                                    new NegativeTextAnalyzer(),
                                    new SpamAnalyzer(new String[]{"sell", "buy"})};
        String text = "asdwdgwgt wtbsrtberb ewferqgr sell =(";
        System.out.println(checkLabels(analyzers, text));
    }

    public static Label checkLabels(TextAnalyzer[] analyzers, String text) {
        for (TextAnalyzer analyzer : analyzers) {
            Label label = analyzer.processText(text);
            if (label != Label.OK) {
                return label;
            }
        }
        return Label.OK;
    }
}
