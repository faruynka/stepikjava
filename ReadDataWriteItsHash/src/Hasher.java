import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.atomic.AtomicBoolean;

public class Hasher extends Thread {
    private ArrayBlockingQueue<String> readerHasherBuffer;
    private ArrayBlockingQueue<Integer> hasherWriterBuffer;
    private AtomicBoolean readerStopped;
    private AtomicBoolean hasherStopped;

    public Hasher(ArrayBlockingQueue<String> readerHasherBuffer, ArrayBlockingQueue<Integer> hasherWriterBuffer,
                  AtomicBoolean readerStopped, AtomicBoolean hasherStopped) {
        this.readerHasherBuffer = readerHasherBuffer;
        this.hasherWriterBuffer = hasherWriterBuffer;
        this.readerStopped = readerStopped;
        this.hasherStopped = hasherStopped;
    }

    @Override
    public void run() {
        try {
            String consumedString;
            while (true) {
                synchronized (readerHasherBuffer) {
                    while (!readerStopped.get() && readerHasherBuffer.isEmpty()) {
                        readerHasherBuffer.wait();
                    }
                    if (readerStopped.get() && readerHasherBuffer.isEmpty()) {
                        break;
                    }
                    consumedString = readerHasherBuffer.poll();
                    readerHasherBuffer.notify();
                }
                int hashcode = consumedString.hashCode();
                synchronized (hasherWriterBuffer) {
                    while (hasherWriterBuffer.remainingCapacity() == 0) {
                        hasherWriterBuffer.wait();
                    }
                    hasherWriterBuffer.add(hashcode);
                    hasherWriterBuffer.notify();
                }
            }
            synchronized (hasherWriterBuffer) {
                hasherStopped.set(true);
                hasherWriterBuffer.notify();
            }
        } catch (InterruptedException e) {
            System.out.println(e.getMessage());
        }
    }
}