import java.io.*;
import java.util.Arrays;

public class Main {
    public static void main(String[] args) throws IllegalArgumentException {
        byte[] byteArray = null;
        try (ByteArrayOutputStream output = new ByteArrayOutputStream();
             ObjectOutputStream oos = new ObjectOutputStream(output)) {
            oos.writeInt(3);
            oos.writeObject(new Animal("Dog"));
            oos.writeObject(new Animal("Tiger"));
            oos.writeObject(new Animal("Mouse"));

            output.flush();
            byteArray = output.toByteArray();

        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.println(Arrays.toString(byteArray));
        Animal[] animals = deserializeAnimalArray(byteArray);
        System.out.println(Arrays.toString(animals));
    }

    public static Animal[] deserializeAnimalArray(byte[] data) throws IllegalArgumentException {
        ByteArrayInputStream arr = new ByteArrayInputStream(data);
        Animal[] res;
        try (ObjectInputStream ois = new ObjectInputStream(arr)) {
            int length = ois.readInt();
            res = new Animal[length];
            for (int i = 0; i < length; i++) {
                Animal deserializedAnimal = (Animal) ois.readObject();
                if (deserializedAnimal instanceof Animal) {
                    res[i] = deserializedAnimal;
                } else {
                    throw new IllegalArgumentException();
                }
            }
        } catch (Exception e) {
            throw new IllegalArgumentException();
        }
        return res;
    }
}

