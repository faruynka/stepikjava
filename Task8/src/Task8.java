import java.util.Arrays;

public class Task8 {
    public static void main(String arg[]) {
        int[] a1 = {1, 1, 1, 2, 13};
        int[] a2 = {1, 8, 9, 9, 12, 14};
        System.out.println(Arrays.toString(mergeArrays(a1, a2)));
    }

    public static int[] mergeArrays(int[] a1, int[] a2) {
        int[] aRes = new int[a1.length + a2.length];
        int i = 0;
        int j = 0;
        while (i < a1.length || j < a2.length) {
            if (i == a1.length || j != a2.length && a1[i] > a2[j]) {
                aRes[i + j] = a2[j];
                j++;
            } else {
                aRes[i + j] = a1[i];
                i++;
            }
        }
        return aRes;
    }
}
