public class MailMessage extends AbstractSendable<String> {
    public MailMessage(String from, String to, String content) {
        super(from, to, content);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MailMessage aMailMessage = (MailMessage) o;
        if (!getContent().equals(aMailMessage.getContent())) return false;

        return true;
    }
}