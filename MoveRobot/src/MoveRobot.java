public class MoveRobot {
    public static void main(String arg[]) {
        Robot robot = new Robot(12, 15, Robot.Direction.DOWN);
        System.out.println(robot.getX());
        System.out.println(robot.getY());
        System.out.println(robot.getDirection());

        moveRobot(robot, 20, 25);
        System.out.println(robot.getX());
        System.out.println(robot.getY());
        System.out.println(robot.getDirection());
    }

    public static void moveRobot(Robot robot, int toX, int toY) {
        int xDifference = robot.getX() - toX;
        int yDifference = robot.getY() - toY;
        if (xDifference > 0) {
            if (robot.getDirection().equals(Robot.Direction.RIGHT)) {
                robot.turnRight();
                robot.turnRight();
            } else if (robot.getDirection().equals(Robot.Direction.DOWN)) {
                robot.turnRight();
            } else if (robot.getDirection().equals(Robot.Direction.UP)) {
                robot.turnLeft();
            }
            for (int i = 0; i < xDifference; i++) {
                robot.stepForward();
            }
        } else if (xDifference < 0) {
            if (robot.getDirection().equals(Robot.Direction.LEFT)) {
                robot.turnRight();
                robot.turnRight();
            } else if (robot.getDirection().equals(Robot.Direction.DOWN)) {
                robot.turnLeft();
            } else if (robot.getDirection().equals(Robot.Direction.UP)) {
                robot.turnRight();
            }
            for (int i = xDifference; i < 0; i++) {
                robot.stepForward();
            }
        }
        if (yDifference > 0) {
            if (robot.getDirection().equals(Robot.Direction.RIGHT)) {
                robot.turnRight();
            } else if (robot.getDirection().equals(Robot.Direction.LEFT)) {
                robot.turnLeft();
            } else if (robot.getDirection().equals(Robot.Direction.UP)) {
                robot.turnLeft();
                robot.turnLeft();
            }
            for (int i = 0; i < yDifference; i++) {
                robot.stepForward();
            }
        } else if (yDifference < 0) {
            if (robot.getDirection().equals(Robot.Direction.RIGHT)) {
                robot.turnLeft();
            } else if (robot.getDirection().equals(Robot.Direction.LEFT)) {
                robot.turnRight();
            } else if (robot.getDirection().equals(Robot.Direction.DOWN)) {
                robot.turnLeft();
                robot.turnLeft();
            }
            for (int i = 0; i > yDifference; i--) {
                robot.stepForward();
            }
        }
    }
}


