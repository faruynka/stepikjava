public abstract class Coffee implements DrinkRecipe {
    @Override
    public String recipe() {
        return "A Recipe of " + getName() + " is: Coffee, Water, Sugar, " + uniqueRecipePart();
    }

    public abstract String uniqueRecipePart();

    public abstract String getName();
}
