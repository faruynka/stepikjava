import java.util.Scanner;

public class Sqrt {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        double res;
        try {
            res = scan.nextInt();
        } catch (IllegalArgumentException e) {
            System.out.println(e.getMessage());
            throw e;
        }
        System.out.println(sqrt(res));
    }

    public static double sqrt(double x) {
        if (x < 0) {
            throw new IllegalArgumentException("Expected non-negative number, got " + x);
        }
        return Math.sqrt(x);
    }
}
