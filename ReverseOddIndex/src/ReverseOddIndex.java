import java.util.Iterator;
import java.util.LinkedList;
import java.util.Scanner;

public class ReverseOddIndex {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        LinkedList<Integer> result = new LinkedList();
        while (scan.hasNextInt()) {
            scan.nextInt();
            if (scan.hasNextInt()) {
                result.add(scan.nextInt());
            }
        }
        Iterator<Integer> iterator = result.descendingIterator();
        while (iterator.hasNext()) {
            System.out.print(iterator.next() + " ");
        }
    }
}
