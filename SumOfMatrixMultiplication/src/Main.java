import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Main {
    public static void main(String[] args) {
        int[][] m1 = generateMatrix(500, 500);
        int[][] m2 = generateMatrix(500, 500);
        try {
            System.out.println(getSumOfMultiplyMatrixUsingThread(m1, m2));
        } catch (InterruptedException e) {
            System.out.println(e.getMessage());
        }
        System.out.println(getSumOfMultiplyMatrix(m1, m2));
    }

    public static int getSumOfMultiplyMatrix(int[][] m1, int[][] m2) {
        if (m1.length == 0 || m1.length != m2.length || m2[0].length != m1[0].length || m1[0].length != m1.length) {
            System.out.println("Please write quadratic matrix");
            return 0;
        }
        MatrixMultiplication matrixMultiplication = new MatrixMultiplication();
        matrixMultiplication.multiplateMatrixAndSumElements(m1, m2, 0, m1.length);
        return matrixMultiplication.getSum();
    }

    public static int getSumOfMultiplyMatrixUsingThread(int[][] m1, int[][] m2) throws InterruptedException {
        if (m1.length == 0 || m1.length != m2.length || m2[0].length != m1[0].length || m1[0].length != m1.length) {
            System.out.println("Please write quadratic matrix");
            return 0;
        }
        MatrixMultiplication matrixMultiplication = new MatrixMultiplication();
        int cores = Runtime.getRuntime().availableProcessors();
        int numberOfThread = Math.min(cores, m1.length);
        int partOfMatrix;
        if (m1.length / cores == 0) {
            partOfMatrix = 1;
        } else {
            partOfMatrix = m1.length / cores;
        }
        List<Thread> threads = new ArrayList<>();
        for (int i = 0; i < numberOfThread; i++) {
            final int startRow = i * partOfMatrix;
            final int rowAmount;
            if (i == numberOfThread - 1) {
                rowAmount = m1.length - startRow;
            } else {
                rowAmount = partOfMatrix;
            }
            Thread thread = new Thread() {
                @Override
                public void run() {
                    matrixMultiplication.multiplateMatrixAndSumElements(m1, m2, startRow, rowAmount);
                }
            };
            threads.add(thread);
            thread.start();
        }
        for (int i = 0; i < threads.size(); i++) {
            threads.get(i).join();
        }
        return matrixMultiplication.getSum();
    }

    public static int[][] generateMatrix(int numberOfRows, int numberOfColumn) {
        Random random = new Random();
        int[][] m = new int[numberOfRows][numberOfColumn];
        for (int i = 0; i < m.length; i++) {
            for (int j = 0; j < m[0].length; j++) {
                m[i][j] = 1 + random.nextInt(5);
            }
        }
        return m;
    }
}