import java.io.*;
import java.util.Scanner;

public class WriteSumOfStream {
    public static void main(String[] args) throws IOException {
        writeSumOfStream("C:/Users/User/Desktop/Test/in.txt", "C:/Users/User/Desktop/Test/out.txt");
    }

    public static void writeSumOfStream(String inputPath, String outputPath) throws IOException {
        try (Reader reader = new FileReader(inputPath);
             Writer writer = new FileWriter(outputPath)) {
            int blocksize;
            double sum = 0;
            while ((blocksize = reader.read()) != -1) {
                StringBuffer sb = new StringBuffer();
                while ((char) blocksize != ' ' && blocksize != -1) {
                    sb.append((char) blocksize);
                    blocksize = reader.read();
                }
                sum += Double.parseDouble(sb.toString());
            }
        } catch (IOException e) {
            throw e;
        }
    }

    public static void writeSumOfStreamUsingScan(String inputPath, String outputPath) throws IOException {
        double sum = 0;
        try (Reader reader = new FileReader(inputPath);
             Writer writer = new FileWriter(outputPath)) {
            Scanner scan = new Scanner(reader);
            while (scan.hasNext()) {
                sum += Double.parseDouble(scan.next());
            }
            writer.write(String.valueOf(sum));
        } catch (IOException e) {
            throw e;
        }
    }
}
