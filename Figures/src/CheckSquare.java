public class CheckSquare implements Processor {
    private int c;
    private int c1;

    public CheckSquare(int c, int c1) {
        this.c = c;
        this.c1 = c1;

    }

    @Override
    public Figure processFigure(Figure figure) {
        if (c1 < 1) {
            throw new IllegalArgumentException("Function required coefficient from 1 to infinity");
        }
        if (figure.square() > c) {
            figure.sizeChanging(1 / c1);
        } else {
            figure.sizeChanging(c1);
        }
        return figure;
    }
}
