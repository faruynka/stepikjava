import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.atomic.AtomicBoolean;

public class Writer extends Thread {
    private ArrayBlockingQueue<Integer> hasherWriterBuffer;
    private String path;
    private AtomicBoolean hasherStopped;

    public Writer(ArrayBlockingQueue<Integer> hasherWriterBuffer, String path, AtomicBoolean hasherStopped) {
        this.hasherWriterBuffer = hasherWriterBuffer;
        this.path = path;
        this.hasherStopped = hasherStopped;
    }

    @Override
    public void run() {
        try (BufferedWriter writer = Files.newBufferedWriter(Paths.get(path), StandardCharsets.UTF_8)) {
            Integer consumedHash;
            while (true) {
                synchronized (hasherWriterBuffer) {
                    while (!hasherStopped.get() && hasherWriterBuffer.isEmpty()) {
                        hasherWriterBuffer.wait();
                    }
                    if (hasherStopped.get() && hasherWriterBuffer.isEmpty()) {
                        break;
                    }
                    consumedHash = hasherWriterBuffer.poll();
                    hasherWriterBuffer.notify();
                }
                writer.write(String.valueOf(consumedHash));
                writer.newLine();
            }
        } catch (IOException e) {
            System.out.println(e.getMessage());
        } catch (InterruptedException e) {
            System.out.println(e.getMessage());
        }
    }
}