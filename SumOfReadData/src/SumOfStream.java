import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

public class SumOfStream {
    public static void main(String[] args) throws IOException{
        byte[] arr = {0x33, 0x45, 0x01};
        InputStream stream = new ByteArrayInputStream(arr);
        System.out.println(checkSumOfStream(stream));
    }
    public static int checkSumOfStream(InputStream inputStream) throws IOException {
        int sumOfStream = 0;
        int byteElement = inputStream.read();
        while (byteElement != -1) {
            sumOfStream = Integer.rotateLeft(sumOfStream, 1) ^ byteElement & 0xFF;
            byteElement = inputStream.read();
        }
        return sumOfStream;
    }
}
