import java.util.stream.IntStream;

public class PseudoRandomStream {
    public static void main(String[] args) {
        pseudoRandomStream(13).forEach(System.out::println);
    }

    public static IntStream pseudoRandomStream(int seed) {
        return IntStream.iterate(seed, n -> mid(n * n));
    }

    public static int mid(int number) {
        return number / 10 % 1000;
    }
}

