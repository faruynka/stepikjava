import java.math.*;

public class Task7 {
    public static void main(String arg[]) {
        System.out.println(factorial(4));
    }

    public static BigInteger factorial(int value) {
        BigInteger multiplication = BigInteger.valueOf(1);
        for (int i = 1; i <= value; i++) {
            multiplication = multiplication.multiply(BigInteger.valueOf(i));
        }
        return multiplication;
    }
}
