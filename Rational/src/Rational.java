import javax.print.attribute.standard.MediaSize;

public class Rational {
    private final int numerator;
    private final int denominator;

    public Rational(int numerator, int denominator) {
        this.numerator = numerator;
        this.denominator = denominator;
    }

    public int numerator() {
        return numerator;
    }

    public int denominator() {
        return denominator;
    }

    public Rational add(Rational r) {
        Rational indeterminate = checkIndeterminateFormAdd(r);
        if (indeterminate != null) {
            return indeterminate;
        }
        Rational result = new Rational(numerator * r.denominator + r.numerator * denominator,
                denominator * r.denominator);
        return result.reduction();
    }

    public Rational subtract(Rational r) {
        Rational indeterminate = checkIndeterminateFormAdd(r);
        if (indeterminate != null) {
            return indeterminate;
        }
        Rational result = new Rational(numerator * r.denominator - r.numerator * denominator,
                denominator * r.denominator);
        return result.reduction();
    }

    public Rational mult(Rational r) {
        Rational indeterminate = checkIndeterminateFormMult(r);
        if (indeterminate != null) {
            return indeterminate;
        }
        Rational result = new Rational(numerator * r.numerator, denominator * r.denominator);
        return result.reduction();
    }

    public Rational div(Rational r) {
        Rational indeterminate = checkIndeterminateFormMult(r);
        if (indeterminate != null) {
            return indeterminate;
        }
        Rational result = new Rational(numerator * r.denominator, denominator * r.numerator);
        return result.reduction();
    }

    public Rational reduction() {
        if (isNaN() || isInf()) {
            return this;
        }
        int nod = nod(numerator, denominator);
        return new Rational(numerator / nod, denominator / nod);

    }

    public double toDouble() {
        return (double) numerator / (double) denominator;
    }

    public boolean isNaN() {
        return numerator == 0 && denominator == 0;
    }

    public boolean isInf() {
        return numerator != 0 && denominator == 0;
    }

    @Override
    public String toString() {
        if (numerator == 0 && denominator == 0) {
            return "NaN";
        }
        if (numerator == 0) {
            return "0";
        }
        if (denominator == 0) {
            return "∞";
        }
        if (numerator == denominator) {
            return "1";
        }
        return Integer.toString(numerator) + " / " + Integer.toString(denominator);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof Rational) {
            Rational other = (Rational) obj;
            if (denominator == 0 || other.denominator == 0) {
                return false;
            }
            if (other.numerator == 0 && numerator == 0) {
                return true;
            }
            if (other.numerator == numerator && other.denominator == denominator) {
                return true;
            }
        }
        return false;
    }

    @Override
    public int hashCode() {
        return Double.hashCode(toDouble());
    }

    private Rational checkIndeterminateFormAdd(Rational r) {
        if (isNaN() || r.isNaN()) {
            return new Rational(0, 0);
        }
        if (isInf() || r.isInf()) {
            return new Rational(1, 0);
        }
        return null;
    }

    private Rational checkIndeterminateFormMult(Rational r) {
        if (isNaN() || r.isNaN()) {
            return new Rational(0, 0);
        }
        if (isInf() && r.isInf()) {
            return new Rational(0, 0);
        }
        if (isInf() || r.isInf()) {
            if (numerator == 0 || r.numerator == 0) {
                return new Rational(0, 0);
            }
            return new Rational(1, 0);
        }
        return null;
    }

    private int nod(int numerator, int denominator) {
        if (numerator > denominator) {
            int tmp = numerator % denominator;
            if (tmp == 0) {
                return denominator;
            } else {
                return nod(tmp, denominator);
            }
        } else if (numerator < denominator) {
            int tmp = denominator % numerator;
            if (tmp == 0) {
                return numerator;
            } else {
                return nod(numerator, tmp);
            }
        } else {
            return numerator;
        }
    }
}