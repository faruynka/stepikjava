import java.util.*;

public class SymmetricDifference {
    public static void main(String[] args) {
        Set<Integer> set1 = new LinkedHashSet<>();
        Set<Integer> set2 = new LinkedHashSet<>();
        set1.add(1);
        set1.add(2);
        set1.add(3);
        set2.add(0);
        set2.add(1);
        set2.add(2);
        symmetricDifference(set1, set2);
    }

    public static <T> Set<T> symmetricDifference(Set<? extends T> set1, Set<? extends T> set2) {
        Set<T> sets1 = new LinkedHashSet(set1);
        sets1.removeAll(set2);
        Set<T> sets2 = new LinkedHashSet(set2);
        sets2.removeAll(set1);
        sets1.addAll(sets2);
        return sets1;
    }
}

