import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.Map.Entry.comparingByKey;

public class TheMostOftenWordsInStream {
    public static void main(String[] args) {
        Stream<String> stream = Stream.of("Lorem ipsum dolor sit amet, consectetur adipiscing elit. " +
                "Sed sodales consectetur purus at faucibus. Donec mi quam, tempor vel ipsum non, faucibus suscipit " +
                "massa. Morbi lacinia velit blandit tincidunt efficitur. Vestibulum eget metus imperdiet sapien " +
                "laoreet faucibus. Nunc eget vehicula mauris, ac auctor lorem. Lorem ipsum dolor sit amet, " +
                "consectetur adipiscing elit. Integer vel odio nec mi tempor dignissim.");
        Map<String, Long> result = stream
                .map(s -> s.toLowerCase().split("[^\\p{L}\\p{Digit}]+"))
                .flatMap(Arrays::stream)
                .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
        result.entrySet().stream()
                .sorted(Map.Entry.<String, Long>comparingByValue().reversed().thenComparing(comparingByKey()))
                .map(Map.Entry::getKey)
                .limit(10).forEachOrdered(System.out::println);
    }
}
