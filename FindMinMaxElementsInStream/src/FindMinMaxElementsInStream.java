import java.util.Comparator;
import java.util.List;
import java.util.function.BiConsumer;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class FindMinMaxElementsInStream {
    public static <T> void findMinMax(Stream<? extends T> stream, Comparator<? super T> order,
                                      BiConsumer<? super T, ? super T> minMaxConsumer) {
        try {
            List<T> list = stream.sorted(order).collect(Collectors.toList());
            minMaxConsumer.accept(list.get(0), list.get(list.size() - 1));
        } catch (Throwable e) {
            minMaxConsumer.accept(null, null);
        }
    }
}
