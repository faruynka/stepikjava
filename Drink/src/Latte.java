public class Latte extends Coffee {
    @Override
    public String uniqueRecipePart() {
        return "Milk";
    }

    @Override
    public String getName() {
        return "Latte";
    }
}
