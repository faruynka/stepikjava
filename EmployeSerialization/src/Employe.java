import java.io.Serializable;
import java.util.Calendar;
import java.util.GregorianCalendar;

public class Employe implements Serializable {
    private String name;
    private String workPosition;
    private int salary;
    private GregorianCalendar dateOfStartWork;

    public Employe(String name, String workPosition, int salary, GregorianCalendar dateOfStartWorking) {
        this.name = name;
        this.workPosition = workPosition;
        this.salary = salary;
        this.dateOfStartWork = dateOfStartWorking;
    }

    @Override
    public String toString() {
        return "\n" + "Name: " + name + ","
                + " work position: " + workPosition + ","
                + " salary: " + salary + ","
                + " date of start work: " + dateOfStartWork.get(Calendar.DAY_OF_MONTH) + "." +
                dateOfStartWork.get(Calendar.MONTH) + "." + dateOfStartWork.get(Calendar.YEAR);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Employe) {
            return name.equals(((Employe) obj).name) && workPosition.equals(((Employe) obj).workPosition) &&
                    salary == ((Employe) obj).salary && dateOfStartWork.equals(((Employe) obj).dateOfStartWork);
        }
        return false;
    }
}
