public class Task2 {
    public static void main(String[] args) {
        System.out.println(leapYearCount(209));
    }

    public static int leapYearCount(int year) {
        return year / 4 + year / 400 - year / 100;
    }
}
