import java.util.*;
import java.util.function.Consumer;

public class MailService<T> implements Consumer<Sendable<T>> {
    private Map<String, List<T>> boxMessages = new HashMap<String, List<T>>() {
        @Override
        public List<T> get(Object key) {
            return super.getOrDefault(key, new LinkedList<>());
        }
    };

    @Override
    public void accept(Sendable<T> mailMessage) {
        List<T> list = new LinkedList<>();
        if (boxMessages.containsKey(mailMessage.getTo())) {
            list = boxMessages.get(mailMessage.getTo());
            list.add(mailMessage.getContent());
            boxMessages.replace(mailMessage.getTo(),
                    boxMessages.get(mailMessage.getTo()), list);
        } else {
            list.add(mailMessage.getContent());
            boxMessages.put(mailMessage.getTo(), list);
        }
    }

    public Map<String, List<T>> getMailBox() {
        return boxMessages;
    }
}