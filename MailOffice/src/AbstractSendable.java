public class AbstractSendable<T> implements Sendable<T> {
    private String from;
    private String to;
    private T content;

    public AbstractSendable(String from, String to, T content) {
        this.from = from;
        this.to = to;
        this.content = content;
    }

    @Override
    public String getTo() {
        return to;
    }

    @Override
    public String getFrom() {
        return from;
    }

    @Override
    public T getContent() {
        return content;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AbstractSendable aSendable = (AbstractSendable) o;

        if (!from.equals(aSendable.from)) return false;
        if (!to.equals(aSendable.to)) return false;

        return true;
    }
}