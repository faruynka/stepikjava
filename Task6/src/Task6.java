public class Task6 {
    public static void main(String arg[]) {
        System.out.println(isPalindrome("Madam, I'm Adam!"));
    }

    public static boolean isPalindrome(String text) {
        text = text.replaceAll("[^a-zA-Z0-9]", "");
        StringBuilder reverseText = new StringBuilder(text).reverse();
        return text.equalsIgnoreCase(reverseText.toString());
    }
}
