import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class Main {
    public static void main(String[] args) {
        int[][] m1 = generateMatrix(500, 500);
        int[][] m2 = generateMatrix(500, 500);
        int[][] m1Res = null;
        try {
            m1Res = multiplyMatrixUsingThread(m1, m2);
        } catch (InterruptedException e) {
            System.out.println(e.getMessage());
        }
        int[][] m2Res = multiplyMatrix(m1, m2);
        System.out.println(Arrays.deepEquals(m1Res, m2Res));
    }

    public static int[][] multiplyMatrix(int[][] m1, int[][] m2) {
        MatrixMultiplication matrixMultiplication = new MatrixMultiplication(m1, m2);
        matrixMultiplication.multiplateMatrix();
        return matrixMultiplication.getMatrixResult();
    }

    public static int[][] multiplyMatrixUsingThread(int[][] m1, int[][] m2) throws InterruptedException {

        MatrixMultiplication matrixMultiplication = new MatrixMultiplication(m1, m2);

        int numberOfThreads = Runtime.getRuntime().availableProcessors();
        List<MatrixThread> threads = new ArrayList<>();
        for (int k = 0; k < numberOfThreads; k++) {
            MatrixThread thread = new MatrixThread();
            thread.setMatrixMultiplication(matrixMultiplication);
            threads.add(thread);
        }
        for (int i = 0; i < threads.size(); i++) {
            threads.get(i).start();
        }
        for (int i = 0; i < threads.size(); i++) {
            threads.get(i).join();
        }

        return matrixMultiplication.getMatrixResult();
    }

    public static int[][] generateMatrix(int numberOfRows, int numberOfColumn) {
        Random random = new Random();
        int[][] m = new int[numberOfRows][numberOfColumn];
        for (int i = 0; i < m.length; i++) {
            for (int j = 0; j < m[0].length; j++) {
                m[i][j] = 1 + random.nextInt(5);
            }
        }
        return m;
    }
}
