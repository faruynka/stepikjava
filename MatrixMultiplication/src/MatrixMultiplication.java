public class MatrixMultiplication {
    private int rowNumber;
    private int columnNumber;
    private int[][] matrixResult;
    private int[][] m1;
    private int[][] m2;

    public MatrixMultiplication(int[][] m1, int[][] m2) {
        this.rowNumber = 0;
        this.columnNumber = 0;
        this.m1 = m1;
        this.m2 = m2;
        matrixResult = new int[m1.length][m1[0].length];
    }

    public void multiplateMatrix() {
        for (; ; ) {
            int myRowNumber;
            int myColumnNumber;
            synchronized (this) {
                if (rowNumber > m1.length - 1) {
                    return;
                }
                myRowNumber = rowNumber;
                myColumnNumber = columnNumber;
                if (columnNumber == m1[0].length - 1) {
                    columnNumber = 0;
                    rowNumber++;
                } else {
                    columnNumber++;
                }
            }
            int element = 0;
            for (int k = 0; k < m1[0].length; k++) {
                element += m1[myRowNumber][k] * m2[k][myColumnNumber];

            }
            matrixResult[myRowNumber][myColumnNumber] = element;
        }
    }

    public int[][] getMatrixResult() {
        synchronized (matrixResult) {
            return matrixResult;
        }
    }
}
