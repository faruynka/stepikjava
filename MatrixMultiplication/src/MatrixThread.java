public class MatrixThread extends Thread {
    private MatrixMultiplication matrixMultiplication;

    @Override
    public void run() {
        matrixMultiplication.multiplateMatrix();
    }

    public void setMatrixMultiplication(MatrixMultiplication matrixMultiplication) {
        this.matrixMultiplication = matrixMultiplication;
    }
}
