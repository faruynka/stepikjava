abstract class KeywordAnalyzer implements TextAnalyzer {
    @Override
    public Label processText(String text) {
        for (String word: getKeywords()) {
            if (text.contains(word)) {
                return getLabel();
            }
        }
        return Label.OK;
    }

    protected abstract String[] getKeywords();
    protected abstract Label getLabel();
}
