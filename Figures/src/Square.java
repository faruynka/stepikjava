public class Square implements Figure {
    private Point bottomLeft;
    private Point upLeft;
    private Point upRight;
    private Point bottomRight;
    private double a;
    double square = square();

    public Square(Point bottomLeft, Point upLeft, Point upRight, Point bottomRight) {
        this.bottomLeft = bottomLeft;
        this.upLeft = upLeft;
        this.upRight = upRight;
        this.bottomRight = bottomRight;
        a = bottomRight.distTo(bottomLeft);
    }

    public double getA() {
        return a;
    }

    @Override
    public String toString() {
        return "bottomLeftPoint: [" + bottomLeft.getX() + ", " + bottomLeft.getY() + "]" + '\n'
                + "upLeftPoint: [" + upLeft.getX() + ", " + upLeft.getY() + "]" + '\n'
                + "upRightPoint: [" + upRight.getX() + ", " + upRight.getY() + "]" + '\n'
                + "bottomLeftPoint: [" + bottomRight.getX() + ", " + bottomRight.getY() + "]";
    }

    @Override
    public void sizeChanging(double coefficient) {
        if (coefficient < 0) {
            throw new IllegalArgumentException("Coefficient is a negative number");
        } else if (coefficient > 1) {
            upRight = new Point(upRight.getX(), a * coefficient - a + upRight.getY());
            upLeft = new Point(a * coefficient - a + upLeft.getX(), a * coefficient - a + upLeft.getY());
            bottomLeft = new Point(a * coefficient - a + bottomLeft.getX(), bottomLeft.getY());
        } else if (coefficient < 1 && coefficient > 0) {
            upRight = new Point(upRight.getX(), upRight.getY() - (a - a * coefficient));
            upLeft = new Point(upLeft.getX() - (a - a * coefficient), upLeft.getY() - (a - a * coefficient));
            bottomLeft = new Point(bottomLeft.getX() - (a - a * coefficient), bottomLeft.getY());
        }
    }

    @Override
    public double perimeter() {
        return 4 * getA();
    }

    @Override
    public double square() {

        return a * a;
    }

    @Override
    public void figureShift(Shift direction, double shift) {
        if (direction.equals(Shift.RIGHT)) {
            bottomRight = new Point(bottomRight.getX() + shift, bottomRight.getY());
            upRight = new Point(upRight.getX() + shift, upRight.getY());
            upLeft = new Point(upLeft.getX() + shift, upLeft.getY());
            bottomLeft = new Point(bottomLeft.getX() + shift, bottomLeft.getY());
        } else if (direction.equals(Shift.LEFT)) {
            bottomRight = new Point(bottomRight.getX() - shift, bottomRight.getY());
            upRight = new Point(upRight.getX() - shift, upRight.getY());
            upLeft = new Point(upLeft.getX() - shift, upLeft.getY());
            bottomLeft = new Point(bottomLeft.getX() - shift, bottomLeft.getY());
        } else if (direction.equals(Shift.UP)) {
            bottomRight = new Point(bottomRight.getX(), bottomRight.getY() + shift);
            upRight = new Point(upRight.getX(), upRight.getY() + shift);
            upLeft = new Point(upLeft.getX(), upLeft.getY() + shift);
            bottomLeft = new Point(bottomLeft.getX(), bottomLeft.getY() + shift);
        } else {
            bottomRight = new Point(bottomRight.getX(), bottomRight.getY() - shift);
            upRight = new Point(upRight.getX(), upRight.getY() - shift);
            upLeft = new Point(upLeft.getX(), upLeft.getY() - shift);
            bottomLeft = new Point(bottomLeft.getX(), bottomLeft.getY() - shift);
        }
    }

    @Override
    public void rotate(double θ) {
        bottomRight = new Point(bottomRight.getX() * Math.cos(θ) - bottomRight.getY() * Math.sin(θ),
                bottomRight.getX() * Math.sin(θ) + bottomRight.getY() * Math.cos(θ));
        bottomLeft = new Point(bottomLeft.getX() * Math.cos(θ) - bottomLeft.getY() * Math.sin(θ),
                bottomLeft.getX() * Math.sin(θ) + bottomLeft.getY() * Math.cos(θ));
        upLeft = new Point(upLeft.getX() * Math.cos(θ) - upLeft.getY() * Math.sin(θ),
                upLeft.getX() * Math.sin(θ) + upLeft.getY() * Math.cos(θ));
        upRight = new Point(upRight.getX() * Math.cos(θ) - upRight.getY() * Math.sin(θ),
                upRight.getX() * Math.sin(θ) + upRight.getY() * Math.cos(θ));
    }
}
