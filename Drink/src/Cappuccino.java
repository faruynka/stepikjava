public class Cappuccino extends Coffee {
    @Override
    public String uniqueRecipePart() {
        return "Milk, Syrup";
    }

    @Override
    public String getName() {
        return "Cappuccino";
    }
}
