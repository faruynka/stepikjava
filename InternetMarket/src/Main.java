import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        Order order = new Order();
        while (true) {
            String productOrder = scan.next();
            if (productOrder.equals("0")) {
                break;
            }
            Product product;
            try {
                product = Factory.productName(productOrder);
            } catch (IllegalArgumentException e) {
                System.out.println("Wrong product name");
                continue;
            }
            order.addProduct(product);
        }
        System.out.println(order.priceOrder());
        System.out.println("X coord is: " + order.packetSize().x + "; Y coord is: " + order.packetSize().y +
                "; Z coord is: " + order.packetSize().z);
    }
}