public class AsciiCharSequence implements CharSequence {
    private final byte[] arr;

    public AsciiCharSequence(byte arr[]) {
        this.arr = arr;
    }

    @Override
    public int length() {
        return arr.length;
    }

    @Override
    public char charAt(int index) {
        return (char) arr[index];
    }

    @Override
    public CharSequence subSequence(int start, int end) {
        byte[] subArray = new byte[end - start];
        for (int i = 0; i < subArray.length; i++) {
            subArray[i] = arr[start + i];
        }
        return new AsciiCharSequence(subArray);
    }

    @Override
    public String toString() {
        return new String(arr);
    }
}
