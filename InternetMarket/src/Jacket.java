public class Jacket implements Product {
    @Override
    public Size size() {
        return new Size(6, 10, 1);
    }

    @Override
    public double price() {
        return 56.3;
    }

    @Override
    public String name() {
        return "Jacket";
    }
}
