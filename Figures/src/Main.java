public class Main {
    public static void main(String[] args) {
        Point bottomLeft = new Point(1, 1);
        Point upLeft = new Point(1, 6);
        Point bottomRight = new Point(6, 1);
        Point upRight = new Point(6, 6);
        Figure square = new Square(bottomLeft, upLeft, upRight, bottomRight);

        Processor[] processors = new Processor[2];
        System.out.println(square.toString());
        System.out.println(square.perimeter());
    }
}
