import java.io.*;
import java.nio.charset.*;

public class ConvertionToString {
    public static void main(String[] args) throws IOException {
        byte[] arr = {48, 49, 50, 51};
        InputStream byteStream = new ByteArrayInputStream(arr);
        System.out.print(readAsString(byteStream, StandardCharsets.US_ASCII));
    }

    public static String readAsString(InputStream inputStream, Charset charset) throws IOException {
        Reader reader = new InputStreamReader(inputStream, charset);
        StringBuffer result = new StringBuffer();
        int blocksize;
        while ((blocksize = reader.read()) != -1) {
            result.append((char) blocksize);
        }
        return result.toString();
    }
}

