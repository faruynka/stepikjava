import java.util.Objects;

public class Pair<T, G> {
    private T value1;
    private G value2;

    private Pair(T value1, G value2) {
        this.value1 = value1;
        this.value2 = value2;
    }

    public T getFirst() {
        return value1;
    }

    public G getSecond() {
        return value2;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || obj.getClass() != this.getClass()) {
            return false;
        }
        Pair<T, G> other = (Pair) obj;
        return Objects.equals(value1, other.value1) && Objects.equals(value2, other.value2);
    }

    @Override
    public int hashCode() {
        return this.value1.hashCode() + this.value2.hashCode();
    }

    public static Pair of(Object value1, Object value2) {
        return new Pair(value1, value2);
    }
}
