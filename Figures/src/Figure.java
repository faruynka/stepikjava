public interface Figure {
    void sizeChanging(double coefficient);
    double perimeter();
    double square();
    void rotate(double θ);
    void figureShift(Shift direction, double coefficient);

}
enum Shift{UP, DOWN, RIGHT, LEFT}