public class Main {
    public static void main(String[] args) {
        Rational r1 = new Rational(1, 0);
        Rational r2 = new Rational(0, 1);
        System.out.println(r1.equals(r2));
        System.out.println(r1.add(r2));
    }
}
