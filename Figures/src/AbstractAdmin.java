public abstract class AbstractAdmin {
    protected Processor[] processor;

    public AbstractAdmin(Processor[] processor) {
        this.processor = processor;
    }

    abstract void process(Figure figure);
}
