public class SysAdmin extends AbstractAdmin {

    public SysAdmin(Processor[] processor) {
        super(processor);
    }

    @Override
    public void process(Figure figure){
        for (int i = 0; i < processor.length; i++) {
            figure = processor[i].processFigure(figure);
        }
        System.out.println(figure.toString());
    }
}
