public class Task9 {
    public static void main(String arg[]) {
        String[] roles = {"Городничий", "Аммос Федорович", "Артемий Филиппович", "Лука Лукич"};
        String[] textLines = {"Городничий: Я пригласил вас, господа, с тем, чтобы сообщить вам " +
                "пренеприятное известие: к нам едет ревизор.", "Аммос Федорович: Как ревизор?",
                "Артемий Филиппович: Как ревизор?", "Городничий: Ревизор из Петербурга, инкогнито." +
                " И еще с секретным предписаньем.", "Аммос Федорович: Вот те на!",
                "Артемий Филиппович: Вот не было заботы, так подай!",
                "Лука Лукич: Господи боже! еще и с секретным предписаньем!"};
        System.out.println(printTextPerRole(roles, textLines));
    }

    public static String printTextPerRole(String[] roles, String[] textLines) {
        StringBuilder result = new StringBuilder();
        for (String role : roles) {
            result.append(role + ":");
            for (int j = 0; j < textLines.length; j++) {
                String[] arr = textLines[j].split(":", 2);
                if (role.equals(arr[0])) {
                    result.append("\n" + (j + 1) + ")" + arr[1]);
                }
            }
            result.append("\n" + "\n");
        }
        return result.toString();
    }
}
