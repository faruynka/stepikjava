import java.util.logging.Logger;

public class LogerSysAdmin extends AbstractAdmin {

    public LogerSysAdmin(Processor[] processor) {
        super(processor);
    }

    @Override
    public void process(Figure figure){
        Logger logger = Logger.getLogger(figure.getClass().getName());
        for (int i = 0; i< processor.length; i++) {
            figure = processor[i].processFigure(figure);
            logger.warning("");
            System.out.println(figure.toString());
        }
        logger.info("");
    }
}