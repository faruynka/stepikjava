public class Robot {
    public enum Direction {
        UP,
        DOWN,
        LEFT,
        RIGHT
    }

    Robot(int x, int y, Direction direction) {
        this.x = x;
        this.y = y;
        this.direction = direction;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public Direction getDirection() {
        return direction;
    }

    public void turnLeft() {
        if (getDirection().equals(Direction.LEFT)) {
            direction = Direction.DOWN;
        } else if (getDirection().equals(Direction.DOWN)) {
            direction = Direction.RIGHT;
        } else if (getDirection().equals(Direction.RIGHT)) {
            direction = Direction.UP;
        } else {
            direction = Direction.LEFT;
        }
    }

    public void turnRight() {
        if (getDirection().equals(Direction.LEFT)) {
            direction = Direction.UP;
        } else if (getDirection().equals(Direction.UP)) {
            direction = Direction.RIGHT;
        } else if (getDirection().equals(Direction.RIGHT)) {
            direction = Direction.DOWN;
        } else {
            direction = Direction.LEFT;
        }
    }


    public void stepForward() {
        if (getDirection().equals(Direction.LEFT)) {
            x--;
        } else if (getDirection().equals(Direction.UP)) {
            y++;
        } else if (getDirection().equals(Direction.RIGHT)) {
            x++;
        } else {
            y--;
        }
    }

    private int x;
    private int y;
    private Direction direction;
}

