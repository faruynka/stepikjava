import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Main {
    public static void main(String[] args) {
        Logger loger = Logger.getLogger(MailMessage.class.getName());
        MailService spy = new Spy(loger);
        MailService thief = new Thief(5);
        MailService inspector = new Inspector();
        MailService[] arr = {thief, spy, inspector};
        MailService badworker = new UntrustworthyMailWorker(arr);
        MailPackage mailPackage = new MailPackage("Austin Powers", "to",
                new Package("weapons", 4));
        MailPackage mailPackage1 = new MailPackage("a", "b", new Package("not a Stone", 8));
        System.out.println(badworker.processMail(mailPackage));
        System.out.println(badworker.processMail(mailPackage1));
    }

    public static interface Sendable {
        String getFrom();

        String getTo();
    }

    public static abstract class AbstractSendable implements Sendable {

        protected final String from;
        protected final String to;

        public AbstractSendable(String from, String to) {
            this.from = from;
            this.to = to;
        }

        @Override
        public String getFrom() {
            return from;
        }

        @Override
        public String getTo() {
            return to;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            AbstractSendable that = (AbstractSendable) o;

            if (!from.equals(that.from)) return false;
            if (!to.equals(that.to)) return false;

            return true;
        }

    }

    public static class MailMessage extends AbstractSendable {

        private final String message;

        public MailMessage(String from, String to, String message) {
            super(from, to);
            this.message = message;
        }

        public String getMessage() {
            return message;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            if (!super.equals(o)) return false;

            MailMessage that = (MailMessage) o;

            if (message != null ? !message.equals(that.message) : that.message != null) return false;

            return true;
        }

    }

    public static class MailPackage extends AbstractSendable {
        private final Package content;

        public MailPackage(String from, String to, Package content) {
            super(from, to);
            this.content = content;
        }

        public Package getContent() {
            return content;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            if (!super.equals(o)) return false;

            MailPackage that = (MailPackage) o;

            if (!content.equals(that.content)) return false;

            return true;
        }

    }

    public static class Package {
        private final String content;
        private final int price;

        public Package(String content, int price) {
            this.content = content;
            this.price = price;
        }

        public String getContent() {
            return content;
        }

        public int getPrice() {
            return price;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            Package aPackage = (Package) o;

            if (price != aPackage.price) return false;
            if (!content.equals(aPackage.content)) return false;

            return true;
        }
    }

    public static interface MailService {
        Sendable processMail(Sendable mail);
    }

    public static class RealMailService implements MailService {

        @Override
        public Sendable processMail(Sendable mail) {
            return mail;
        }
    }

    public static class UntrustworthyMailWorker implements MailService {
        private MailService[] mailProcessing;
        private RealMailService realMail = new RealMailService();

        public UntrustworthyMailWorker(MailService[] mailProcessing) {
            this.mailProcessing = mailProcessing;
        }

        public RealMailService getRealMailService() {
            return realMail;
        }

        @Override
        public Sendable processMail(Sendable mail) {
            for (MailService agent: mailProcessing) {
                mail = agent.processMail(mail);
            }
            return realMail.processMail(mail);
        }
    }

    public static class Spy implements MailService {
        private Logger loger;

        public Spy(Logger loger) {
            this.loger = loger;
        }

        @Override
        public Sendable processMail(Sendable mail) {
            if (mail instanceof MailMessage) {
                if (mail.getFrom().equals("Austin Powers") || mail.getTo().equals("Austin Powers")) {
                    loger.log(Level.WARNING, "Detected target mail correspondence: from {0} to {1} \"{2}\"",
                            new Object[]{mail.getFrom(), mail.getTo(), ((MailMessage) mail).getMessage()});
                } else {
                    loger.log(Level.INFO, "Usual correspondence: from {0} to {1}",
                            new Object[]{mail.getFrom(), mail.getTo()});
                }
            }
            return mail;
        }

    }

    public static class Thief implements MailService {
        private int minPrice;
        private ArrayList<Integer> stolenPackages = new ArrayList<>();

        public Thief(int minPrice) {
            this.minPrice = minPrice;
        }

        public int getStolenValue() {
            int stolenValue = 0;
            for (int i = 0; i < stolenPackages.size(); i++) {
                stolenValue += stolenPackages.get(i);
            }
            return stolenValue;
        }

        @Override
        public Sendable processMail(Sendable mail) {
            if (mail instanceof MailPackage) {
                MailPackage mailPackage = (MailPackage) mail;
                if (mailPackage.getContent().getPrice() >= minPrice) {
                    stolenPackages.add(mailPackage.getContent().getPrice());
                    mail = new MailPackage(mailPackage.from, mailPackage.getTo(),
                            new Package("stones instead of " + mailPackage.getContent().getContent(), 0));
                }
            }
            return mail;
        }
    }

    public static class Inspector implements MailService {
        @Override
        public Sendable processMail(Sendable mail) {
            if (mail instanceof MailPackage) {
                MailPackage mailPackage = (MailPackage) mail;
                if (mailPackage.getContent().getContent().toLowerCase().contains("weapons")
                        || mailPackage.getContent().getContent().toLowerCase().contains("banned substance")) {
                    throw new IllegalPackageException();
                } else if (mailPackage.getContent().getContent().toLowerCase().contains("stones")) {
                    throw new StolenPackageException();
                }
            }
            return mail;
        }
    }

    public static class IllegalPackageException extends RuntimeException {
    }

    public static class StolenPackageException extends RuntimeException {
    }
}
