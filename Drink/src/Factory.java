public class Factory {
    public static DrinkRecipe getRecipe(String name) {
        if (name.equals("Latte")) {
            return new Latte();
        } else if (name.equals("Cappuccino")) {
            return new Cappuccino();
        } else if (name.equals("Mojito")) {
            return new Mojito();
        } else if (name.equals("PinaColada")) {
            return new PinaColada();
        } else {
            throw new IllegalArgumentException("Wrong name of drink");
        }
    }
}
