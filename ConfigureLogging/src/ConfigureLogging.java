import java.util.logging.*;

public class ConfigureLogging {
    private static void configureLogging() {
        Logger LOGGER_CLASS_A = Logger.getLogger("org.stepic.java.logging.ClassA");
        Logger LOGGER_CLASS_B = Logger.getLogger("org.stepic.java.logging.ClassB");
        LOGGER_CLASS_A.setLevel(Level.ALL);
        LOGGER_CLASS_B.setLevel(Level.WARNING);

        Logger logger = Logger.getLogger("org.stepic.java");
        // parents can't get messages
        logger.setUseParentHandlers(false);

        // Specify a ConsoleHanlder for this logger instance.
        Handler handler = new ConsoleHandler();
        // set level for ConsoleHandler
        handler.setLevel(Level.ALL);
        logger.addHandler(handler);

        Formatter formatter = new XMLFormatter();
        handler.setFormatter(formatter);
    }
}

