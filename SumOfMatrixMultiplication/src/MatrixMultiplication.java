public class MatrixMultiplication {
    private int sum = 0;

    public int getSum() {
        return sum;
    }

    public void multiplateMatrixAndSumElements(int[][] m1, int[][] m2, int startRow, int rowsAmount) {
        for (int i = startRow; i < startRow + rowsAmount; i++) {
            for (int j = 0; j < m1.length; j++) {
                int element = 0;
                for (int k = 0; k < m1[0].length; k++) {
                    element += m1[i][k] * m2[k][j];
                }
                synchronized (this) {
                    sum += element;
                }
            }
        }
    }
}