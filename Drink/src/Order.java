import java.util.ArrayList;

public class Order {
    private ArrayList<DrinkRecipe> recipes = new ArrayList<>();

    public String allRecipes() {
        StringBuilder recipeString = new StringBuilder();
        for (DrinkRecipe recipe : recipes) {
            recipeString.append(recipe.recipe() + "\n");
        }
        return recipeString.toString();
    }

    public void addRecipe(DrinkRecipe recipe) {
        recipes.add(recipe);
    }
}
