import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        Order order = new Order();
        while (true) {
            String orderElement = scan.next();
            if (orderElement.equals("0")) {
                break;
            }
            DrinkRecipe recipe;
            try {
                recipe = Factory.getRecipe(orderElement);
            } catch (IllegalArgumentException e) {
                System.out.println("Wrong drink name.");
                continue;
            }
            order.addRecipe(recipe);
        }
        System.out.println(order.allRecipes());
    }
}
