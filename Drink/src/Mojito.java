public class Mojito extends HotDrink {
    @Override
    public String uniqueRecipePart() {
        return "Mint, Lime, Sprite";
    }

    @Override
    public String getName() {
        return "Mojito";
    }
}
