import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.atomic.AtomicBoolean;

public class Main {
    public static void main(String[] args) {
        String inputPath = "/home/faruynka/Nastya/input.txt";
        String outputPath = "/home/faruynka/Nastya/output.txt";
        computeHash(inputPath, outputPath);
    }

    public static void computeHash(String inputPath, String outputPath) {
        AtomicBoolean readerStopped = new AtomicBoolean();
        readerStopped.set(false);
        AtomicBoolean hasherStopped = new AtomicBoolean();
        hasherStopped.set(false);
        ArrayBlockingQueue<String> readerHasherBuffer = new ArrayBlockingQueue<>(5);
        ArrayBlockingQueue<Integer> hasherWriterBuffer = new ArrayBlockingQueue<>(5);
        Reader readThread = new Reader(readerHasherBuffer, inputPath, readerStopped);
        Hasher hashThread = new Hasher(readerHasherBuffer, hasherWriterBuffer, readerStopped, hasherStopped);
        Writer writeThread = new Writer(hasherWriterBuffer, outputPath, hasherStopped);
        readThread.start();
        hashThread.start();
        writeThread.start();
        try {
            readThread.join();
            hashThread.join();
            writeThread.join();
        } catch (InterruptedException e) {
            System.out.println(e.getMessage());
        }
    }

}

