public class Salary extends AbstractSendable<Integer> {
    public Salary(String from, String to, Integer salary) {
        super(from, to, salary);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Salary aSalary = (Salary) o;
        if (getContent() != aSalary.getContent()) return false;

        return true;
    }
}